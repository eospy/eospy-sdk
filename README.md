# EOSPY #



### What is EOSPY? ###

**EOSPY** is a **Python** library and SDK for integration with **EOSIO**-based blockchains using the **EOSIO RPC API**.

**EOSPY** provides a **Python** interface for interaction between **Django** web applications and **EOSIO** blockchains. 